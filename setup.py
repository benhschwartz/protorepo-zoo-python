
from setuptools import setup

setup(
    name='zoo',
    version='0.3.128',
    description='Generated protobuf package for zoo',
    url='https://bitbucket.org/benhschwartz/protorepo-zoo-python',
    author='Shippo',
    author_email='circleci@goshippo.com',
    license='unlicense',
    packages=['zoo'],
    zip_safe=False
)
